# Github Trending

Frontend Coding Challenge SPA

# The Techs Stack

-   Typescript
-   React / react-query / usehooks-ts
-   UI - Bootstrap + React
-   SASS

# Run

To run the application as local development

```
> npm install (or yarn install)
> npm run dev (or yarn dev)
```

Or to build a production version

```
> npm install --production (or yarn install --production)
> npm run build (or yarn build)
```

# Hosting

The running web application is currently hosted under GitLab Page
http://nevtio.com.gitlab.io/github-trending (Non-HTTPS)

# Source code

The source code could be access from GitLab https://gitlab.com/nevtio.com/github-trending

The target build code could be found under the "Release" section https://gitlab.com/nevtio.com/github-trending/-/releases

# ToDo / Known Issues

Following items are currently omitted from the implementations:

-   Unit / Integration Test
-   Code refactoring / Needs better state management perhaps (e.g. Redux / Recoil)
