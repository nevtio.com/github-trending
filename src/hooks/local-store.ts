import { useLocalStorage } from 'usehooks-ts'

export const useRepoType = () =>{
    const [repoType, setRepoType] = useLocalStorage<string>('repoType', '/all')

    const updateRepoType = (repoType:string) =>{
        setRepoType(repoType)
    }

    return { repoType, updateRepoType }
}

export const useLikedRepos = () => {

    const [likedRepos, setLikedRepos] = useLocalStorage<any[]>('likedRepos', [])

    const toggleLikedRepo = (repo:any) => {
        setLikedRepos((prevRepos)=>{
            const cloned = [...prevRepos]
            const existingIndex = cloned.findIndex(_repo => _repo.id === repo.id)
            if ( existingIndex < 0 )
                cloned.push(repo)
            else
                cloned.splice(existingIndex, 1)
            return cloned
        })
    }

    return { likedRepos, toggleLikedRepo }
}

export const useFilteringLangs = () => {

    const [filteringLangs, setFilteringLangs] = useLocalStorage<string[]>('filteringLangs', [])

    const toggleFilteringLangs = (filteringLang:string) => {
        setFilteringLangs((prevFilteringLangs)=>{
            const cloned = [...prevFilteringLangs]
            const existingIndex = cloned.indexOf(filteringLang)
            if ( existingIndex < 0 )
                cloned.push(filteringLang)
            else
                cloned.splice(existingIndex, 1)
            return cloned
        })
    }

    return { filteringLangs, toggleFilteringLangs }
}