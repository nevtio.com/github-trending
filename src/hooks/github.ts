import { useEffect, useState } from 'react'
import { useQuery, useQueryClient } from 'react-query'
import { searchStarsLast7Days } from '../services/github'

export const useGithubSearchFetch = ({ limit=30, fromDate }:{limit:number, fromDate:string} = { limit:30, fromDate:'' }) => {

    const [state, setState] = useState<{
        page: number, repos: any[]
    }>({
        page: 1,
        repos: []
    })

    const queryClient = useQueryClient()
    const { status, data, error } = useQuery(['fetch', { page:state.page, fromDate }], async () => await searchStarsLast7Days(state.page, limit, fromDate), { refetchOnWindowFocus: false, cacheTime:1 })

    useEffect(()=>{
        if ( status === 'success' )
            setState((prevState)=>{
                const repos = [...prevState.repos]
                repos[prevState.page] = (data as any).items
                return { page:prevState.page, repos }
            })
    }, [data, status, state.page])

    const reset = ()=>{
        if ( status === 'success' ) {
            queryClient.invalidateQueries('fetch')
            setState(()=>{
                return { page:1, repos:[] }
            })
        }
    }

    const next = ()=>{
        if ( status === 'success' )
            setState((prevState)=>{
                return { page:prevState.page + 1, repos:prevState.repos }
            })
    }

    return { status, state, error, reset, next }
}