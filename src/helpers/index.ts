export const debounce = (func: any, timeout = 300) => {
    let timer:NodeJS.Timeout
    return (...args: any[]) => {
        clearTimeout(timer)
        return new Promise((resolve, reject)=>{
            timer = setTimeout(async () => {
                try {
                    resolve(await func.apply(this, args))
                } catch(error) {
                    reject(error)
                }
            }, timeout)
        })

    }
}