import axios from 'axios'
import { debounce } from '../helpers'
import { GithubSearchData } from '../types'

export const getApiEndpointUrl = () => {
    return (import.meta.env.VITE_API_ENDPOINT_URL ?? window.location.href) as string
}

export const searchStarsLast7Days = debounce(async (page=1, limit=30, fromDate=''):Promise<GithubSearchData> => {

    const baseURL = getApiEndpointUrl()
    const url = '/search/repositories'
    const res = await axios({
        method: 'GET',
        baseURL,
        url,
        params: {
            q:`created:>${fromDate}`,
            sort: 'stars',
            order: 'desc',
            page: `${page}`,
            per_page: `${limit}`
        }
    })
    return res.data

    // const res = await import('../data/github-search.json')
    // return res

}, 1)