import { QueryClient, QueryClientProvider } from 'react-query'
import GithubTrending from './components/GithubTrending'

function App() {

    const queryClient = new QueryClient()

    return (
        <div className="app">
            <QueryClientProvider client={queryClient}>
                <GithubTrending />
            </QueryClientProvider>
        </div>
    )
}

export default App
