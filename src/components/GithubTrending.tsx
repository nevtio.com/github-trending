import dayjs from 'dayjs'
import { useMemo } from 'react'
import { Button, Card, Col, Container, Row } from 'react-bootstrap'
import { useBottomScrollListener } from 'react-bottom-scroll-listener'
import { useGithubSearchFetch } from '../hooks/github'
import { useFilteringLangs, useLikedRepos, useRepoType } from '../hooks/local-store'
import GithubRepoListBody from './GithubRepoListBody'
import GithubRepoListHeader from './GithubRepoListHeader'
import GithubTrendingHeroHeader from './GithubTrendingHeroHeader'

export const GithubTrending = () => {

    // Last 7 days date as YYYY-MM-DD
    const last7daysDate = dayjs().subtract(7, 'day').format('YYYY-MM-DD')

    // Local Storage
    const { repoType } = useRepoType()
    const { likedRepos } = useLikedRepos()
    const { filteringLangs } = useFilteringLangs()

    // Github API
    const { state, reset, next, status } = useGithubSearchFetch({ fromDate:last7daysDate, limit:30 })

    // Concat the repos as 1 dimension array
    const repos = useMemo(() => state.repos.filter(repo => !!repo).flat(), [state.repos])

    // Handle the bottom scrolled
    useBottomScrollListener(()=>{
        if ( status === 'success' && repoType === '/all' )
            next()
    }, {
        triggerOnNoScroll: true
    })

    // Event handlers
    const onRefreshButtonClicked = ()=>{
        reset()
    }
    const onShowMoreButtonClicked = () =>{
        next()
    }

    // (DOM-UI) Event handlers
    const onScrollBackToTopButtonClicked = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth' // for smoothly scrolling
        })
    }


    return (
        <Container className="github-trending pb-5">
            <GithubTrendingHeroHeader status={status} onRefresh={onRefreshButtonClicked} />
            <Row className="repos-body pt-5 ps-3 pe-3">
                <Col>
                    <Card className="shadow">
                        <GithubRepoListHeader displayingDate={last7daysDate} repoType={repoType} likedRepos={likedRepos} filteringLangs={filteringLangs} />
                        <GithubRepoListBody repoType={repoType} repos={repos} likedRepos={likedRepos} filteringLangs={filteringLangs} status={status} />
                    </Card>

                    {repoType === '/all' ? <div className="d-grid gap-2">
                        <br />
                        <Button onClick={onShowMoreButtonClicked} disabled={status === 'loading'}>Show More</Button>
                        <br />
                    </div> : '' }

                    <div className="fixed-bottom p-3 d-flex justify-content-end">
                        <Button className="back-top-button rounded-circle shadow" onClick={onScrollBackToTopButtonClicked}>&#8593;</Button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}
export default GithubTrending