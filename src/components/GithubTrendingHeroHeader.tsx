import { Button, Col, Row, Spinner } from 'react-bootstrap'
import logoUrl from '../imgs/GitHub_Logo_White.png'
import octocatUrl from '../imgs/Octocat.png'

export interface GithubTrendingHeroHeaderProps {
    status: string;
    onRefresh: () => void;
}

export const GithubTrendingHeroHeader = (props: GithubTrendingHeroHeaderProps) => {

    const isLoading = props.status === 'loading'
    const onRefreshButtonClick = () => {
        props.onRefresh()
    }

    return (
        <div className="hero-header">
            <Row>
                <Col md={6} className="p-3 text-center text-md-start">
                    <img className="logo" src={logoUrl} />
                    <div className="ps-3">
                        <h1 className="title">Github Trending</h1>
                        <p className="pe-5">A list of the repositories created in the last 7 days with the most number of stars in github</p>
                    </div>
                    <div className="pt-5 ps-2 d-grid d-md-block">
                        <Button className="d-flex justify-content-center align-items-center" disabled={isLoading} size="lg" onClick={onRefreshButtonClick}><span>Refresh</span> {isLoading ? <Spinner className="ms-3" animation="border" /> : ''}</Button>
                    </div>
                    <img className="octocat d-md-none" src={octocatUrl} />
                </Col>
            </Row>

        </div>
    )
}
export default GithubTrendingHeroHeader