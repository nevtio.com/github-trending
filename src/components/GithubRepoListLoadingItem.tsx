import { ListGroup, Placeholder, Spinner } from 'react-bootstrap'

export interface GithubRepoListLoadingItemProps {
    data?:any
}

export const GithubRepoListLoadingItem = (props:GithubRepoListLoadingItemProps) => {
    return (<ListGroup.Item as="li" className="repo loading p-0 d-block d-md-flex  align-items-stretch">
        <div className="p-3 text-center text-md-start">
            <Spinner className="m-3 ms-md-5" variant="light" animation="border" />
        </div>

        <div className="p-3 ps-md-0 flex-grow-1">
            <div className="d-flex align-items-center">
                <Placeholder as="h5" xs={5} bg="light" animation="glow">&nbsp;</Placeholder>
            </div>
            <Placeholder as="p" xs={12} bg="light" animation="glow">&nbsp;</Placeholder>
        </div>

        <div className="actions pb-5">
            <ul>
                <li className="p-3">
                    <Placeholder as="label" xs={12} bg="light" animation="glow">&nbsp;</Placeholder>
                </li>
                <li className="p-3 pt-0 d-flex">
                    <Placeholder.Button className="flex-grow-1" variant="outline-light">LIKE</Placeholder.Button>
                </li>
            </ul>
        </div>
    </ListGroup.Item>)
}

export default GithubRepoListLoadingItem