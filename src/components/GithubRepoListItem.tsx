import { Badge, Button, ListGroup } from 'react-bootstrap'
import { useFilteringLangs, useLikedRepos } from '../hooks/local-store'
import githubStarIconUrl from '../imgs/github-star.png'

export interface GithubRepoListItemProps {
    repo: any;
    isLiked: boolean;
    isFiltered: boolean;
    isShowingRank: boolean;
}

export const GithubRepoItemList = (props:GithubRepoListItemProps) =>{

    const { toggleLikedRepo } = useLikedRepos()
    const { toggleFilteringLangs } = useFilteringLangs()

    const onLikeButtonClick = () => {
        toggleLikedRepo(props.repo)
    }
    const onLangFilterClicked = () => {
        toggleFilteringLangs(props.repo.language)
    }

    return <ListGroup.Item as="li" className={`repo p-0 d-block d-md-flex align-items-stretch ${props.isFiltered ? 'filtered' : ''} ${props.isShowingRank ? '' : 'no-rank'}`} active={props.isLiked}>
        <div className="p-3 text-center text-md-start">
            <img
                src={props.repo.owner.avatar_url}
                className="ms-3 avatar rounded-circle border shadow"
                alt="Avatar"
            />
        </div>
        <div className="p-3 ps-md-0 flex-grow-1" style={{ minWidth: 0 }}>
            <div className="d-flex align-items-center position-relative">
                <h5 className="text-truncate">
                    <a href={props.repo.html_url} target="_blank">{props.repo.full_name}</a>
                </h5>
                <Badge className="ms-1" role="button" pill bg="info" onClick={onLangFilterClicked}>{props.repo.language}</Badge>
            </div>
            <p>{props.repo.description ?? 'No description'}</p>
        </div>
        <div className="actions pb-5">
            <ul>
                <li className="p-3">
                    <label className="d-flex flex-nowrap align-items-center justify-content-center justify-md-content-start"><img className="github-star-icon me-1" src={githubStarIconUrl} /><span className="fw-bold pe-2">STAR</span> <span className="fw-bold text-primary">{props.repo.stargazers_count}</span></label>
                </li>
                <li className="p-3 pt-0 d-flex">
                    <Button className="flex-grow-1" variant={props.isLiked ? 'primary' : 'outline-primary'} onClick={onLikeButtonClick}>{props.isLiked ? 'UNLIKE' : 'LIKE' }</Button>
                </li>
            </ul>
        </div>
    </ListGroup.Item>
}
export default GithubRepoItemList