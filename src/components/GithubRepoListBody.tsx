import { ListGroup } from 'react-bootstrap'
import GithubRepoListItem from './GithubRepoListItem'
import GithubRepoListLoadingItem from './GithubRepoListLoadingItem'

export interface GithubRepoListBodyProps {
    repoType: string
    repos: any[];
    likedRepos: any[];
    filteringLangs: string[];
    status: string;
}

export const GithubRepoListBody = (props:GithubRepoListBodyProps) =>{

    const currentRepos = props.repoType === '/liked' ? props.likedRepos : props.repos
    const isShowingRank = props.repoType === '/all'

    return (
        <>
            <ListGroup className="repos" as="ol" variant="flush" numbered>
                {currentRepos.map((repo:any, index:number)=>{
                    const isLiked = (props.likedRepos.findIndex(_repo => _repo.id === repo.id) >= 0)
                    const isFiltered = (props.filteringLangs.length > 0 && !props.filteringLangs.includes(repo.language) )

                    // Each repo item
                    return <GithubRepoListItem key={`${repo.id}.${index}`} isShowingRank={isShowingRank} repo={repo} isLiked={isLiked} isFiltered={isFiltered} />
                })}
                { props.status === 'loading' && (
                    // When loading is happening, show the optimistic loading section
                    <><GithubRepoListLoadingItem /><GithubRepoListLoadingItem /><GithubRepoListLoadingItem /><GithubRepoListLoadingItem /><GithubRepoListLoadingItem /></>)
                }
                { props.status === 'success' && currentRepos.length === 0 ? (
                    // When no repos existing, show empty row
                    <ListGroup.Item as="li" className="repo p-0 no-rank"><div className="p-5 text-center">Nothing found</div></ListGroup.Item> ) : ''
                }

            </ListGroup>
        </>
    )
}
export default GithubRepoListBody