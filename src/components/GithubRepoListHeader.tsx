import { Badge, Card, Nav } from 'react-bootstrap'
import { useFilteringLangs, useRepoType } from '../hooks/local-store'

export interface GithubRepoListHeaderProps {
    displayingDate: string
    repoType: string
    likedRepos: any[]
    filteringLangs: string[]
}

export const GithubRepoListHeader = (props: GithubRepoListHeaderProps) => {

    const { updateRepoType } = useRepoType()
    const { toggleFilteringLangs } = useFilteringLangs()

    // Event handlers
    const onReposFilterSelected = (eventKey:string | null) => {
        if ( eventKey )
            updateRepoType(eventKey)
    }
    const onFilteringLangBadgeClicked = (filteringLang:string) => {
        toggleFilteringLangs(filteringLang)
    }

    return (
        <>
            <Card.Header className="pt-3">
                <p className="">Most number of STARS since <strong>{props.displayingDate}</strong></p>
                <div className="d-flex align-items-center justify-content-between">
                    <Nav
                        className="repos-filter-nav"
                        variant="tabs"
                        defaultActiveKey="/all"
                        activeKey={props.repoType}
                        onSelect={onReposFilterSelected}>
                        <Nav.Item>
                            <Nav.Link eventKey="/all" >All Repos</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link eventKey="/liked" >
                                <span>Liked Repos</span>
                                { props.likedRepos.length > 0 ? <Badge className="ms-1" pill bg="primary">{props.likedRepos.length}</Badge> : '' }
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>

                    <div className="sticky-top">
                        {props.filteringLangs.length > 0 ? <div className="ms-3">
                            <span>Filtering:</span>
                            {
                                props.filteringLangs.map((filteringLang)=>{
                                    return <Badge key={filteringLang} className="ms-1" role="button" pill bg="info" onClick={() => onFilteringLangBadgeClicked(filteringLang)}>{filteringLang}</Badge>
                                })
                            }
                        </div> : ''}
                    </div>
                </div>
            </Card.Header>
        </>
    )
}

export default GithubRepoListHeader