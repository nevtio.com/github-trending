export interface GithubSearchData {
    total_count: number,
    incomplete_results: boolean,
    items: any[],
}