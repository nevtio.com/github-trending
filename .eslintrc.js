module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint', 'react-hooks'],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    env: {
        node: true,
        jest: true,
    },
    rules: {
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': [
            'warn', {
                'additionalHooks': '(useRecoilCallback|useRecoilTransaction_UNSTABLE)'
            }
        ],
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-unused-expressions':
            process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-empty-function':
            process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-unused-vars':
            process.env.NODE_ENV === 'production' ? 'error' : 'off',

        'linebreak-style': ['error', 'unix'],
        indent: ['error', 4, { SwitchCase: 1, flatTernaryExpressions: true }],
        semi: ['error', 'never'],
        quotes: ['warn', 'single'],
        curly: ['error', 'multi', 'consistent'],
        'prefer-promise-reject-errors': 'off',
        'multiline-ternary': ['error', 'never'],
        'brace-style': ['error', '1tbs', { allowSingleLine: true }],
        // 'array-element-newline': ['error', { multiline: true, minItems: 3 }],
        'array-bracket-newline': ['error', { multiline: true }],
        'object-curly-spacing': ['error', 'always'],

        '@typescript-eslint/interface-name-prefix': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/require-await': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/indent': [
            'error',
            4,
            { SwitchCase: 1, flatTernaryExpressions: true },
        ],
        '@typescript-eslint/no-unused-vars':
            process.env.NODE_ENV === 'production' ? 'error' : 'off',
    },
}
