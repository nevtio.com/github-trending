import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import * as path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    base: '/github-trending/',
    build: {
        target: 'es2020',
        outDir: './dist',
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, '/src'),
            '~bootstrap': 'bootstrap',
            '~bootswatch': 'bootswatch'
        }
    }
})
